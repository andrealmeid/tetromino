/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "app.h"

#include <SDL.h>
#include <SDL_ttf.h>
#include <time.h>

#include "color.h"
#include "game.h"

#define FONT_NAME "Cantarell-Regular.ttf"

struct App {
  SDL_Window *window;
  SDL_Surface *surface;
  TTF_Font *font;

  bool is_running;

  int font_size;

  uint32_t w, h;

  SDL_GameController *controller;

  bool fullscreen;

  Game *game;
};

App *app_new() {
  App *self = malloc(sizeof(App));
  return self;
}

bool app_is_running(App *self) { return self->is_running; }

bool app_init(App *self) {
  self->is_running = true;
  self->w = DEFAULT_SCREEN_WIDTH;
  self->h = DEFAULT_SCREEN_HEIGHT;
  self->controller = NULL;
  self->fullscreen = false;

  srand((unsigned int)time(NULL));

  /*
   * Initialize Font Engine and load the font
   */
  if (TTF_Init() == -1) {
    fprintf(stderr, "Unable to initialize SDL_ttf: %s\n", TTF_GetError());
    return false;
  }

  /*
   * Initialize SDL Video and Audio
   */
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_GAMECONTROLLER) < 0) {
    fprintf(stderr, "Unable to init SDL: %s\n", SDL_GetError());
    return false;
  }

  self->window = SDL_CreateWindow(
      "Tetromino", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
      (int)self->w, (int)self->h, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
  if (self->window == NULL) {
    fprintf(stderr, "Window could not be created! SDL_Error: %s\n",
            SDL_GetError());
    return false;
  }

  self->surface = SDL_GetWindowSurface(self->window);

  SDL_SetWindowIcon(self->window, SDL_LoadBMP("sdlblocks.bmp"));

  self->game = game_new();

  Board *board = game_get_board(self->game);
  board_update_from_extent(board, DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT);

  self->font_size = board_get_unit_px(board);
  self->font = TTF_OpenFont(FONT_NAME, self->font_size);
  if (self->font == NULL) {
    fprintf(stderr, "Unable to load font file: %s %s\n", FONT_NAME,
            TTF_GetError());
    return false;
  }

  return true;
}

void app_finalize(App *self) {
  game_finalize(self->game);
  free(self->game);
  if (self->controller) SDL_GameControllerClose(self->controller);
  SDL_FreeSurface(self->surface);
  TTF_CloseFont(self->font);
  TTF_Quit();
  SDL_Quit();
}

static void action_start(App *self) { game_start(self->game); }

static void action_quit(App *self) { self->is_running = false; }

static void _set_size(App *self, uint32_t w, uint32_t h) {
  self->w = w;
  self->h = h;
  self->surface = SDL_GetWindowSurface(self->window);

  Board *board = game_get_board(self->game);
  board_update_from_extent(board, (uint16_t)self->w, (uint16_t)self->h);

  self->font_size = board_get_unit_px(board);

  TTF_CloseFont(self->font);
  self->font = TTF_OpenFont(FONT_NAME, self->font_size);
  if (self->font == NULL) {
    fprintf(stderr, "Unable to load font file: %s %s\n", FONT_NAME,
            TTF_GetError());
  }
}

static void _process_window_event(App *self, SDL_WindowEvent event) {
  switch (event.event) {
    case SDL_WINDOWEVENT_SIZE_CHANGED:
      _set_size(self, (uint32_t)event.data1, (uint32_t)event.data2);
      break;
    default:
      break;
  }
}

#define SDL_CONTROLLER_BUTTON_SOUTH SDL_CONTROLLER_BUTTON_A
#define SDL_CONTROLLER_BUTTON_EAST SDL_CONTROLLER_BUTTON_B
#define SDL_CONTROLLER_BUTTON_WEST SDL_CONTROLLER_BUTTON_X
#define SDL_CONTROLLER_BUTTON_NORTH SDL_CONTROLLER_BUTTON_Y

static void _process_controller_event(App *self, SDL_ControllerButtonEvent e) {
  switch (e.button) {
    case SDL_CONTROLLER_BUTTON_SOUTH:
      game_rotate_cw(self->game);
      break;
    case SDL_CONTROLLER_BUTTON_EAST:
      game_rotate_ccw(self->game);
      break;
    case SDL_CONTROLLER_BUTTON_WEST:
      break;
    case SDL_CONTROLLER_BUTTON_NORTH:
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_UP:
      game_request_hard_drop(self->game);
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_DOWN:
      game_soft_drop(self->game);
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_LEFT:
      game_move_left(self->game);
      break;
    case SDL_CONTROLLER_BUTTON_DPAD_RIGHT:
      game_move_right(self->game);
      break;
    case SDL_CONTROLLER_BUTTON_START:
      action_start(self);
      break;
    case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER:
      break;
    default:
      break;
  }
}

static void _process_game_key_event(Game *game, SDL_Keycode sym) {
  if (!game_has_started(game)) return;

  switch (sym) {
    case SDLK_LEFT:
      game_move_left(game);
      break;
    case SDLK_RIGHT:
      game_move_right(game);
      break;
    case SDLK_UP:
    case SDLK_x:
      game_rotate_cw(game);
      break;
    case SDLK_z:
    case SDLK_LCTRL:
      game_rotate_ccw(game);
      break;
    case SDLK_DOWN:
      game_soft_drop(game);
      break;
    case SDLK_SPACE:
      game_request_hard_drop(game);
      break;
    default:
      break;
  }
}

static void _process_key_event(App *self, SDL_Keycode sym) {
  _process_game_key_event(self->game, sym);

  switch (sym) {
    case SDLK_RETURN:
      action_start(self);
      break;
    case SDLK_ESCAPE:
      action_quit(self);
      break;
    case SDLK_f:
      self->fullscreen = !self->fullscreen;
      SDL_SetWindowFullscreen(
          self->window, self->fullscreen ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0);
      break;
    default:
      break;
  }
}

static void _process_event(App *self, SDL_Event event) {
  switch (event.type) {
    case SDL_QUIT:
      action_quit(self);
      break;
    case SDL_WINDOWEVENT:
      _process_window_event(self, event.window);
      break;
    case SDL_KEYDOWN:
      _process_key_event(self, event.key.keysym.sym);
      break;
    case SDL_CONTROLLERBUTTONDOWN:
      _process_controller_event(self, event.cbutton);
      break;
    case SDL_CONTROLLERDEVICEADDED:
      if (!self->controller) {
        self->controller = SDL_GameControllerOpen(event.cdevice.which);
        if (self->controller)
          printf("Found controller %d: %s\n", event.cdevice.which,
                 SDL_GameControllerName(self->controller));
        else
          fprintf(stderr, "Could not open controller %i: %s\n",
                  event.cdevice.which, SDL_GetError());
      }
      break;
    case SDL_CONTROLLERDEVICEREMOVED:
      if (self->controller) {
        printf("Detaching controller %d: %s\n", event.cdevice.which,
               SDL_GameControllerName(self->controller));
        SDL_GameControllerClose(self->controller);
        self->controller = NULL;
      }
      break;
    default:
      break;
  }
}

static void _poll_events(App *self) {
  SDL_Event event;
  while (SDL_PollEvent(&event)) _process_event(self, event);
}

/*
 * game_draw_text
 *
 * draw a text string to some surface at some (x,y)
 *
 */
static void _draw_line(App *self, int x, int y, char *text, SDL_Color white) {
  SDL_Surface *src = TTF_RenderText_Solid(self->font, text, white);
  if (src == NULL) return;

  SDL_Rect rect = {.x = x, .y = y, .w = src->w, .h = src->h};

  SDL_BlitSurface(src, NULL, self->surface, &rect);
  SDL_FreeSurface(src);
}

static void _draw_text(App *self) {
  SDL_Color white = {0xff, 0xff, 0xff, 0};

  char text[256];

  Board *board = game_get_board(self->game);

  uint16_t x = board_get_max_x(board) + board_get_unit_px(board);
  uint16_t y = board_get_y(board) + board_get_unit_px(board);

  GameState *state = game_get_state(self->game);

  _draw_line(self, x, y, "Tetromino", white);
  y += self->font_size * 3;
  sprintf(&text[0], "level: %d", state->level);
  _draw_line(self, x, y, &text[0], white);
  y += self->font_size;
  sprintf(&text[0], "lines: %d", state->num_lines_cleared);
  _draw_line(self, x, y, &text[0], white);
  y += self->font_size;
  sprintf(&text[0], "score: %d", state->score);
  _draw_line(self, x, y, &text[0], white);
  y += self->font_size * 3;

  if (state->is_paused)
    _draw_line(self, x, y, "PAUSE", white);
  else if (!state->has_started)
    _draw_line(self, x, y, "PRESS RETURN...", white);
  else if (state->is_over)
    _draw_line(self, x, y, "GAME OVER", white);
}

static void _draw(App *self) {
  if (SDL_MUSTLOCK(self->surface)) SDL_LockSurface(self->surface);

  /* Draw the background */
  SDL_FillRect(self->surface, NULL, COLOR_BLACK);

  game_draw(self->game, self->surface);

  _draw_text(self);

  if (SDL_MUSTLOCK(self->surface)) SDL_UnlockSurface(self->surface);

  SDL_UpdateWindowSurface(self->window);
}

void app_iterate(App *self) {
  _poll_events(self);
  game_iterate(self->game);
  _draw(self);
}
