/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <SDL.h>
#include <stdbool.h>
#include <stdint.h>

#include "board.h"
#include "config.h"
#include "queue.h"
#include "tetromino.h"

typedef struct {
  bool is_over;
  bool is_paused;
  bool has_started;

  uint32_t level;
  uint32_t level_speed;

  uint32_t score;
  uint32_t num_lines_cleared;
} GameState;

typedef struct Game Game;

Game *game_new(void);
void game_finalize(Game *self);

void game_iterate(Game *self);
void game_start(Game *self);
void game_request_hard_drop(Game *self);
Board *game_get_board(Game *self);

void game_move_left(Game *self);
void game_move_right(Game *self);
void game_rotate_cw(Game *self);
void game_rotate_ccw(Game *self);
void game_soft_drop(Game *self);
void game_draw(Game *self, SDL_Surface *surface);
bool game_has_started(Game *self);
GameState *game_get_state(Game *self);
