/*
 * Tetromino
 *
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct Queue Queue;

/* Game API */
Queue *queue_new_with_bag(void);
uint8_t queue_pop_next(Queue *self);

/* Just used internally, exposed for tests */
uint8_t *random_bag_new(void);

Queue *queue_new(void);
uint8_t queue_peek(Queue *self, uint8_t at);
void queue_push_tail(Queue *self, uint8_t data);
uint8_t queue_pop_head(Queue *self);
