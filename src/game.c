/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "game.h"

#include "color.h"
#include "config.h"

struct Game {
  bool is_hard_dropping;

  GameState state;

  uint32_t last_tick;

  Board *board;

  Tetromino *current;
  Queue *queue;
};

static void _init_state(GameState *state) {
  state->is_over = false;
  state->is_paused = false;
  state->has_started = false;

  state->score = 0;
  state->num_lines_cleared = 0;

  state->level = 0;
  state->level_speed = 500;
}

GameState *game_get_state(Game *self) { return &self->state; }

static void _init(Game *self) {
  self->is_hard_dropping = false;

  self->last_tick = SDL_GetTicks();

  self->current = NULL;

  _init_state(&self->state);

  self->board = board_new();

  self->queue = queue_new_with_bag();
}

/*
 * Reinit everything besides board
 * alternative would be to cache the window size
 * */
static void _reinit(Game *self) {
  free(self->queue);
  if (self->current) free(self->current);
  self->is_hard_dropping = false;
  self->last_tick = SDL_GetTicks();
  self->current = NULL;
  _init_state(&self->state);
  board_reset(self->board);
  self->queue = queue_new_with_bag();
}

Board *game_get_board(Game *self) { return self->board; }

Game *game_new() {
  Game *self = malloc(sizeof(Game));
  _init(self);
  return self;
}

void game_draw(Game *self, SDL_Surface *surface) {
  board_draw(self->board, surface);
  if (self->state.has_started) tetromino_draw(self->current, surface);
}

bool game_has_started(Game *self) { return self->state.has_started; }

void game_finalize(Game *self) {
  free(self->queue);
  free(self->board);
  if (self->current) free(self->current);
}

static void _spawn_tetromino(Game *self) {
  if (self->current) free(self->current);

  self->current = tetromino_new(queue_pop_next(self->queue), self->board);

  /* check for game over */
  if (!tetromino_is_position_valid(self->current)) self->state.is_over = true;

#ifdef DEBUG_GAME
  board_print(self->board);
#endif
}

void game_start(Game *self) {
  if (self->state.is_over) {
    _reinit(self);
  } else if (!self->state.has_started) {
    self->state.has_started = true;
  }
  _spawn_tetromino(self);
}

void game_request_hard_drop(Game *self) { self->is_hard_dropping = true; }

/*
 * game_score
 *
 * calculate game score based on NES scoring algorithm
 * see wikipedia.org entry for game for an explaination.
 *
 */
static uint32_t _score(uint32_t level, uint32_t lines) {
  if (lines == 0)
    return 0;
  else if (lines == 1) {
    return (level + 1) * 40;
  } else if (lines == 2) {
    return (level + 1) * 100;
  } else if (lines == 3) {
    return (level + 1) * 300;
  } else {
    return (level + 1) * 1200;
  }
}

static void _state_update_score(GameState *state, uint32_t num_lines_cleared) {
  state->score += _score(state->level, num_lines_cleared);
  state->num_lines_cleared += num_lines_cleared;

  /* check for tilt, if so then reset the score */
  if (state->score > 9999999) state->score = 0;
}

/*
 * game_level_up
 *
 * increase the game level every 10 lines cleared
 * stop at level 20
 *
 * increase level_speed by 20ms per level
 *
 */
static void _level_up(GameState *state) {
  if ((state->num_lines_cleared > 9) && (state->level < 20)) {
    state->level++;
    state->level_speed -= 20;
    state->num_lines_cleared -= 10;
  } else if ((state->num_lines_cleared >= 10) && (state->level >= 20)) {
    state->level++;
    state->num_lines_cleared -= 10;
  }
}

static void _check_clear(Game *self) {
  uint32_t num_lines_cleared = board_update(self->board);
  _state_update_score(&self->state, num_lines_cleared);
  _level_up(&self->state);

#ifdef DEBUG_GAME
  fprintf(stderr, "\n");
  fprintf(stderr, "cur level: %d\n", self->state.level);
  fprintf(stderr, "cur score: %d\n", self->state.score);
  fprintf(stderr, "num lines cleared: %d\n", self->state.num_lines_cleared);
  fprintf(stderr, "\n");
#endif
}

static void _on_collide(Game *self, int8_t prev_y) {
  /* move the tetromino back */
  tetromino_set_y(self->current, prev_y);
  self->is_hard_dropping = false;
  tetromino_put(self->current);
  _check_clear(self);
  _spawn_tetromino(self);
}

/* move down one row */
static void _perform_down_move(Game *self) {
  int8_t prev_y = tetromino_get_y(self->current);

  tetromino_move_down(self->current);

  if (tetromino_get_y(self->current) > tetromino_get_max_y(self->current))
    _on_collide(self, prev_y); /* Tetromino reached board bottom */

  /* make sure we can move into this position */
  if (!tetromino_is_position_valid(self->current)) {
    if (tetromino_get_y(self->current) < 0) {
      self->state.is_over = true;
    } else {
      _on_collide(self, prev_y); /* Tetromino collided with other */
    }
  }
}

static void _wait_for_tick(Game *self, uint32_t timeout) {
  if ((SDL_GetTicks() - self->last_tick) < timeout) return;

  self->last_tick = SDL_GetTicks();
  _perform_down_move(self);
}

#define HARD_DROP_TIMEOUT 10

void game_iterate(Game *self) {
  if (!self->state.has_started || self->state.is_over) return;
  _wait_for_tick(self, self->is_hard_dropping ? HARD_DROP_TIMEOUT
                                              : self->state.level_speed);
}

void game_move_left(Game *self) {
  if (!self->is_hard_dropping) tetromino_move_left(self->current);
}

void game_move_right(Game *self) {
  if (!self->is_hard_dropping) tetromino_move_right(self->current);
}

void game_rotate_cw(Game *self) {
  if (!self->is_hard_dropping) tetromino_rotate_cw(self->current);
}

void game_rotate_ccw(Game *self) {
  if (!self->is_hard_dropping) tetromino_rotate_ccw(self->current);
}

void game_soft_drop(Game *self) {
  if (!self->is_hard_dropping) tetromino_soft_drop(self->current);
}
