/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdint.h>

typedef struct {
  uint8_t w, h;
  uint8_t mask_arr[6];
} TetrominoMask;

typedef struct {
  char id;
  uint8_t num_rotation_states;
  uint32_t color;
  TetrominoMask *mask;
} TetrominoType;
