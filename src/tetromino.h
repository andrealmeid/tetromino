/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdbool.h>

#include "board.h"

typedef struct Tetromino Tetromino;

Tetromino *tetromino_new(uint8_t type_id, Board *board);

void tetromino_draw(Tetromino *self, SDL_Surface *surface);
void tetromino_put(Tetromino *self);
bool tetromino_is_position_valid(Tetromino *self);

void tetromino_move_left(Tetromino *self);
void tetromino_move_right(Tetromino *self);
void tetromino_rotate_cw(Tetromino *self);
void tetromino_rotate_ccw(Tetromino *self);
void tetromino_soft_drop(Tetromino *self);
uint8_t tetromino_get_max_y(Tetromino *self);

void tetromino_move_down(Tetromino *self);
int8_t tetromino_get_y(Tetromino *self);
void tetromino_set_y(Tetromino *self, int8_t y);
bool tetromino_can_move_to(Tetromino *self, int8_t y, int8_t x);
void tetromino_draw_at(Tetromino *self, SDL_Surface *surface, int8_t y,
                       int8_t x, uint32_t color);
