/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#define DEFAULT_SCREEN_WIDTH 1280
#define DEFAULT_SCREEN_HEIGHT 720

#define BOARD_WIDTH 10
#define BOARD_HEIGHT 20
