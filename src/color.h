/*
 * Tetromino
 *
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdint.h>

// clang-format off
static const uint32_t COLOR_GREEN   = 0x00ff00;
static const uint32_t COLOR_RED     = 0xff0000;
static const uint32_t COLOR_BLUE    = 0x0000ff;
static const uint32_t COLOR_CYAN    = 0x00ffff;
static const uint32_t COLOR_YELLOW  = 0xffff00;
static const uint32_t COLOR_MAGENTA = 0xff00ff;
static const uint32_t COLOR_WHITE   = 0xffffff;
static const uint32_t COLOR_ORANGE  = 0xff7f00;
static const uint32_t COLOR_GRAY_50 = 0x808080;
static const uint32_t COLOR_GRAY_25 = 0x404040;
static const uint32_t COLOR_BLACK   = 0x000000;
// clang-format on

typedef struct {
  uint8_t r, g, b;
} ColorRGB;

ColorRGB hex2rgb(uint32_t hex);
uint32_t rgb2hex(uint8_t r, uint8_t g, uint8_t b);
