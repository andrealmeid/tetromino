/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "board.h"

#include "color.h"
#include "config.h"

struct Board {
  uint16_t unit_px;
  /* Extent in pixels */
  uint16_t w, h;
  /* Position in pixels */
  uint16_t x, y;

  uint32_t grid_color;

  /* Board grid with uint32_t color values */
  uint32_t state[BOARD_HEIGHT][BOARD_WIDTH];
};

void board_reset(Board *self) {
  uint32_t *state_ptr = board_get_state_ptr(self);
  memset(state_ptr, 0, (BOARD_HEIGHT * BOARD_WIDTH) * sizeof(uint32_t));
}

Board *board_new() {
  Board *self = malloc(sizeof(Board));
  self->grid_color = COLOR_GRAY_25;
  board_reset(self);
  return self;
}

static uint16_t _get_max_x(Board *self) { return self->x + self->w; }

static uint16_t _get_max_y(Board *self) { return self->y + self->h; }

uint16_t board_get_unit_px(Board *self) { return self->unit_px; }

uint16_t board_get_max_x(Board *self) { return _get_max_x(self); }

uint16_t board_get_y(Board *self) { return self->y; }

uint16_t board_get_x(Board *self) { return self->x; }

uint32_t board_get_state(Board *self, uint8_t y, uint8_t x) {
  return self->state[y][x];
}

void board_set_state(Board *self, uint8_t y, uint8_t x, uint32_t color) {
  self->state[y][x] = color;
}

void board_update_from_extent(Board *self, uint16_t w, uint16_t h) {
  self->unit_px = (uint16_t)((float)h / (float)(BOARD_HEIGHT + 2));

  self->w = self->unit_px * BOARD_WIDTH;
  self->h = self->unit_px * BOARD_HEIGHT;

  self->x = (uint16_t)((float)w / 2.0f - (float)self->w / 2.0f);
  self->y = self->unit_px;
}

/* draw the tetrominoes already on the matrix */
static void _draw_state(Board *self, SDL_Surface *surface) {
  SDL_Rect rect = {
      .h = self->unit_px - 1,
      .w = self->unit_px - 1,
  };

  for (int i = 0; i < BOARD_HEIGHT; i++) {
    rect.y = self->y + i * self->unit_px;
    for (int j = 0; j < BOARD_WIDTH; j++) {
      rect.x = self->x + j * self->unit_px;
      uint32_t c = self->state[i][j];
      if (c) SDL_FillRect(surface, &rect, c);
    }
  }
}

static void hline(SDL_Surface *surface, int x, int y, int width,
                  uint32_t color) {
  SDL_Rect rect = {.x = x, .y = y, .h = 1, .w = width};
  SDL_FillRect(surface, &rect, color);
}

static void vline(SDL_Surface *surface, int x, int y, int height,
                  uint32_t color) {
  SDL_Rect rect = {.x = x, .y = y, .h = height, .w = 1};
  SDL_FillRect(surface, &rect, color);
}

/* draw the walls: left, right, bottom */
static void _draw_borders(Board *self, SDL_Surface *surface) {
  vline(surface, self->x, self->y, self->h, COLOR_WHITE);
  vline(surface, _get_max_x(self), self->y, self->h, COLOR_WHITE);
  hline(surface, self->x, _get_max_y(self), self->w, COLOR_WHITE);
  hline(surface, self->x, self->y, self->w, COLOR_WHITE);
}

/* draw the grid */
static void _draw_grid(Board *self, SDL_Surface *surface) {
  for (int i = 1; i < BOARD_HEIGHT; i++) {
    int y = self->y + (i * self->unit_px);
    for (int j = 1; j < BOARD_WIDTH; j++) {
      int x = self->x + (j * self->unit_px);
      SDL_Rect rect = {.x = x, .y = y, .h = 2, .w = 2};
      SDL_FillRect(surface, &rect, self->grid_color);
    }
  }
}

void board_draw(Board *self, SDL_Surface *surface) {
  _draw_borders(self, surface);
  _draw_grid(self, surface);
  _draw_state(self, surface);
}

uint32_t *board_get_state_ptr(Board *self) { return &(self->state[0][0]); }

void board_print(Board *self) {
  printf("== BOARD ==\n");
  for (uint8_t h = 0; h < BOARD_HEIGHT; h++) {
    for (uint8_t w = 0; w < BOARD_WIDTH; w++)
      printf("%s ", self->state[h][w] > 0 ? "☐" : ".");
    printf("\n");
  }
}

uint32_t board_update(Board *self) {
  uint32_t board_tmp[BOARD_HEIGHT][BOARD_WIDTH];
  memset(&board_tmp[0][0], 0, (BOARD_HEIGHT * BOARD_WIDTH) * sizeof(uint32_t));

  uint32_t num_lines_cleared = 0;

  /* check the board for filled rows from the bottom-up */
  for (int i = BOARD_HEIGHT - 1; i > -1; i--) {
    uint32_t *bptr = (i * BOARD_WIDTH) + board_get_state_ptr(self);
    int n = 0;
    for (int j = 0; j < BOARD_WIDTH; j++)
      if (*bptr++) n++;

#ifdef DEBUG_GAME
    fprintf(stderr, "n: %d\n", n);
#endif

    /* is there a fully filled row? */
    if (n == BOARD_WIDTH) {
      /* yes, then shift the board down to the current level */

      num_lines_cleared++;

      bptr = board_get_state_ptr(self);

      uint32_t m = (uint32_t)i * BOARD_WIDTH;
      memcpy(&board_tmp[0][0], bptr, m * sizeof(uint32_t));
      memcpy(bptr + BOARD_WIDTH, &board_tmp[0][0], m * sizeof(uint32_t));

      /* push the row counter back up to the previous line */
      i++;
    }
  }
  return num_lines_cleared;
}
