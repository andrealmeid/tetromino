/*
 * Tetromino
 *
 * Copyright 2005 Don E. Llopis <llopis.don@gmail.com>
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "tetromino.h"

#include "config.h"
#include "tetromino-type.h"
#include "tetromino-types.h"

struct Tetromino {
  uint8_t rotation;

  /* Position in board coordinates */
  int8_t x;
  int8_t y;

  const TetrominoType *type;

  Board *board;
};

Tetromino *tetromino_new(uint8_t type_id, Board *board) {
  Tetromino *self = malloc(sizeof(Tetromino));
  self->rotation = 0;
  self->board = board;
  self->type = &tetromino_types[type_id];

  self->x = 4;
  self->y = 0;
  return self;
}

void tetromino_move_down(Tetromino *self) { self->y += 1; }

int8_t tetromino_get_y(Tetromino *self) { return self->y; }

void tetromino_set_y(Tetromino *self, int8_t y) { self->y = y; }

static TetrominoMask *_get_mask(Tetromino *self) {
  return &self->type->mask[self->rotation];
}

static uint8_t _get_max_x(Tetromino *self) {
  TetrominoMask *mask = _get_mask(self);
  return BOARD_WIDTH - mask->w;
}

uint8_t tetromino_get_max_y(Tetromino *self) {
  TetrominoMask *mask = _get_mask(self);
  return BOARD_HEIGHT - mask->h;
}

static uint8_t _get_max_y(Tetromino *self) { return tetromino_get_max_y(self); }

static int _get_x_px(Tetromino *self, int8_t x) {
  return board_get_x(self->board) + board_get_unit_px(self->board) * x;
}

static int _get_y_px(Tetromino *self, int8_t y) {
  return board_get_y(self->board) + board_get_unit_px(self->board) * y;
}

void tetromino_draw_at(Tetromino *self, SDL_Surface *surface, int8_t y,
                       int8_t x, uint32_t color) {
  int y_px = _get_y_px(self, y);
  int x_px = _get_x_px(self, x);

  SDL_Rect rect = {.h = board_get_unit_px(self->board) - 1,
                   .w = board_get_unit_px(self->board) - 1};

  TetrominoMask *mask = _get_mask(self);
  uint8_t *m = mask->mask_arr;

  for (int i = 0; i < mask->h; i++) {
    rect.y = y_px + (i * board_get_unit_px(self->board));

    if (y_px > -1)
      for (int j = 0; j < mask->w; j++) {
        rect.x = x_px + (j * board_get_unit_px(self->board));
        if (*m++) SDL_FillRect(surface, &rect, color);
      }
  }
}

static void _draw_ghost(Tetromino *self, SDL_Surface *surface) {
  int8_t y = self->y;
  while (y < _get_max_y(self) && tetromino_can_move_to(self, y + 1, self->x))
    y++;

  if (y == self->y) return;

  ColorRGB rgb = hex2rgb(self->type->color);
  uint32_t ghost_color = rgb2hex(rgb.r / 4, rgb.g / 4, rgb.b / 4);
  tetromino_draw_at(self, surface, y, self->x, ghost_color);
}

void tetromino_draw(Tetromino *self, SDL_Surface *surface) {
  _draw_ghost(self, surface);
  tetromino_draw_at(self, surface, self->y, self->x, self->type->color);
}

bool tetromino_can_move_to(Tetromino *self, int8_t y, int8_t x) {
  TetrominoMask *mask = _get_mask(self);
  uint8_t *m = mask->mask_arr;

  for (int i = 0; i < mask->h; i++) {
    for (int j = 0; j < mask->w; j++) {
      uint32_t s =
          board_get_state(self->board, (uint8_t)(y + i), (uint8_t)(x + j));
      if (*m && s) return false;
      m++;
    }
  }

  return true;
}

bool tetromino_is_position_valid(Tetromino *self) {
  return tetromino_can_move_to(self, self->y, self->x);
}

void tetromino_put(Tetromino *self) {
  if (self->y < 0) return;

  TetrominoMask *mask = _get_mask(self);
  uint8_t *m = mask->mask_arr;

  for (int i = 0; i < mask->h; i++) {
    for (int j = 0; j < mask->w; j++) {
      if (*m)
        board_set_state(self->board, (uint8_t)(self->y + i),
                        (uint8_t)(self->x + j), self->type->color);
      m++;
    }
  }
}

void tetromino_move_left(Tetromino *self) {
  if (tetromino_is_position_valid(self)) {
    int8_t prev_x = self->x;

    self->x -= 1;

    if (self->x < 0) self->x = 0;

    /* make sure we can move into this position */
    if (!tetromino_is_position_valid(self))
      self->x = prev_x; /* move the tetromino back */
  }
}

void tetromino_move_right(Tetromino *self) {
  if (tetromino_is_position_valid(self)) {
    int8_t prev_x = self->x;

    self->x += 1;

    uint8_t max_x = _get_max_x(self);
    if (self->x > max_x) self->x = (int8_t)max_x;

    /* make sure we can move into this position */
    if (!tetromino_is_position_valid(self))
      self->x = prev_x; /* move the tetromino back */
  }
}

static void _rotate(Tetromino *self, int8_t direction) {
  uint8_t last_rotation = self->rotation;

  if (direction == 1) {
    /* cw */
    self->rotation += direction;
    if (self->rotation == self->type->num_rotation_states) self->rotation = 0;
  } else if (direction == -1) {
    /* ccw */
    if (self->rotation == 0)
      self->rotation = self->type->num_rotation_states - 1;
    else
      self->rotation += direction;
  } else {
    fprintf(stderr, "Illegal direction %d provided\n", direction);
    return;
  }

  /*
   * check the position of the new tetromino
   * make sure that it can be placed along
   * the right edge of the game self->board
   */
  if (self->x > _get_max_x(self)) self->rotation = last_rotation;

  if (self->y > _get_max_y(self)) self->rotation = last_rotation;

  if (!tetromino_is_position_valid(self))
    self->rotation = last_rotation; /* move the tetromino back */
}

void tetromino_rotate_cw(Tetromino *self) { _rotate(self, 1); }

void tetromino_rotate_ccw(Tetromino *self) { _rotate(self, -1); }

void tetromino_soft_drop(Tetromino *self) {
  int8_t prev_y = self->y;
  self->y += 1;
  if (self->y > _get_max_y(self)) self->y = prev_y;
  /* make sure we can move into this position */
  if (!tetromino_is_position_valid(self))
    self->y = prev_y; /* move the tetromino back */
}
