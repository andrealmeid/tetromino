/*
 * Tetromino
 *
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <stdbool.h>

typedef struct App App;

App *app_new(void);
bool app_init(App *self);
void app_iterate(App *self);
void app_finalize(App *self);
bool app_is_running(App *self);
