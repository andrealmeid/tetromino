/*
 * Tetromino
 *
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "color.h"

ColorRGB hex2rgb(uint32_t hex) {
  // clang-format off
  return (ColorRGB){
    .r = (hex >> 16) & 0xff,
    .g = (hex >> 8) & 0xff,
    .b = hex & 0xff
  };
  // clang-format on
}

uint32_t rgb2hex(uint8_t r, uint8_t g, uint8_t b) {
  // clang-format off
  return (uint32_t)(((r & 0xff) << 16) |
                    ((g & 0xff) << 8) |
                    (b & 0xff));
  // clang-format on
}
