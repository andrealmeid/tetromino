/*
 * Tetromino
 *
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "queue.h"

#include <stdio.h>
#include <stdlib.h>

#define BAG_LEN 7
uint8_t *random_bag_new() {
  uint8_t array[BAG_LEN];
  for (uint8_t i = 0; i < BAG_LEN; i++) array[i] = i;

  uint8_t *result = malloc(BAG_LEN * sizeof(uint8_t));

  for (uint8_t len = BAG_LEN; len > 1; len--) {
    uint8_t pick = rand() % len;
    result[BAG_LEN - len] = array[pick];

    uint8_t i = 0;
    for (uint8_t j = 0; j < len; j++)
      if (j != pick) {
        if (i != j) array[i] = array[j];
        i++;
      }
  }
  /* Last element */
  result[BAG_LEN - 1] = array[0];

  return result;
}

// 6 + 7
#define MAX_QUEUE 13

struct Queue {
  uint8_t types[MAX_QUEUE];
  int8_t head;
  int8_t tail;
  uint8_t len;
};

Queue *queue_new() {
  Queue *self = malloc(sizeof(Queue));
  self->head = 0;
  self->tail = -1;
  self->len = 0;
  return self;
}

static void _push_bag(Queue *self) {
  uint8_t *bag = random_bag_new();
  for (uint8_t i = 0; i < 7; i++) queue_push_tail(self, bag[i]);
  free(bag);
}

Queue *queue_new_with_bag() {
  Queue *self = queue_new();
  _push_bag(self);
  return self;
}

uint8_t queue_pop_next(Queue *self) {
  uint8_t next = queue_pop_head(self);
  if (self->len < 7) _push_bag(self);
  return next;
}

uint8_t queue_peek(Queue *self, uint8_t at) {
  uint8_t i = (self->head + at) % MAX_QUEUE;
  return self->types[i];
}

void queue_push_tail(Queue *self, uint8_t last) {
  if (self->len == MAX_QUEUE) {
    fprintf(stderr, "Queue is full! Can't add more.\n");
    return;
  }

  if (self->tail == MAX_QUEUE - 1) self->tail = -1;

  self->types[++self->tail] = last;
  self->len++;
}

uint8_t queue_pop_head(Queue *self) {
  if (self->len == 0) {
    fprintf(stderr, "Queue empty, returning 0!\n");
    return 0;
  }

  uint8_t first = self->types[self->head++];

  if (self->head == MAX_QUEUE) self->head = 0;

  self->len--;
  return first;
}
