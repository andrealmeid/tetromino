# Tetromino

A FOSS C implementation of the game "Tetromino Tennis" which aims to be standard conformant.

Based on [SDLBlocks](https://github.com/llopisdon/sdlblocks) by Don E. Llopis.

## Features

* Bag randomizer
* Standard color scheme
* Standard key bindings
* Both rotation directions
* Controller support
* Queue
* Resolution independent
* Ghost piece

## TODO

* Next visualization
* Hold
* SRS (Standard Rotation System)
* Input and latency improvements
* Libretro core implementation
* OpenGL rendering
* Hotseat multiplayer

## Dependencies

- C compiler (clang or gcc)
- meson
- SDL2 and SDL2_ttf

### ArchLinux

```shell
$ pacman -S meson sdl2 sdl2_ttf clang pkgconfig
```

## Build

```shell
$ meson build
$ ninja -C build
```

## Run

```shell
$ ./build/tetromino
```

## Run tests

```shell
$ ninja -C build test
```

## License

GPLv3 or higher.
