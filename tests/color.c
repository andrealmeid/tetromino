/*
 * Tetromino
 *
 * Copyright 2020 Lubosz Sarnecki <lubosz@gmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "color.h"

#include <assert.h>
#include <stdint.h>
#include <stdio.h>

int main() {
  uint32_t hex = 0x123456;
  printf("0x%x\n", hex);

  ColorRGB rgb = hex2rgb(hex);
  printf("0x%x 0x%x 0x%x\n", rgb.r, rgb.g, rgb.b);

  uint32_t new_hex = rgb2hex(rgb.r, rgb.g, rgb.b);

  printf("0x%x\n", new_hex);

  assert(hex == new_hex);

  return 0;
}
